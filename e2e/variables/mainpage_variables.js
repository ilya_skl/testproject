'use strict';

const mainpageVariables = {
  appTitleText: 'OpenOceanStudio: Crew Applications',
};

module.exports = mainpageVariables;