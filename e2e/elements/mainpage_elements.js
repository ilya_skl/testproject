'use strict';

const mainpageElements = {
  appTitle: $('.App-title'),
  filtersForm: $('#filters'),
  nameInput: $("[name='name']"),
  cityInput: $("[name='city']"),
  submitBtn: $("[type='submit']"),
  clearBtn: $("#filters [type='button']"),
  appliedColumnHeader: $(".App-container .App-column:nth-of-type(1) h2"),
  interviewingColumnHeader: $(".App-container .App-column:nth-of-type(2) h2"),
  hiredColumnHeader: $(".App-container .App-column:nth-of-type(3) h2"),
  appliedColumnElements: $$(".App-container .App-column:nth-of-type(1) > div > div"),
  interviewingColumnElements: $$(".App-container .App-column:nth-of-type(2) > div > div"),
  hiredColumnElements: $$(".App-container .App-column:nth-of-type(3) > div > div")
};

module.exports = mainpageElements;