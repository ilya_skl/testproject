'use strict;'

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;

const mainpageVariables = require('../variables/mainpage_variables');
const mainpageElements = require('../elements/mainpage_elements');
const mainpageUtils = require('../utils/mainpage_utils');

  describe('Main page tests', async () => {
    
    beforeEach(async () => {
    await browser.get(browser.baseUrl);
    await browser.wait(EC.visibilityOf(mainpageElements.appTitle), 10000);
    });

    it('TC 1', async () => {
      expect(await mainpageUtils.getBannerText()).to.equal(mainpageVariables.appTitleText);
    });

    it('TC 2', async () => {
      expect(await mainpageUtils.checkSubmitBtn()).to.equal("Submit");
      expect(await mainpageUtils.checkClearBtn()).to.equal("Clear");
    });

    it('TC 3', async () => {
      expect(await mainpageUtils.checkAppliedColumnHeader()).to.equal("Applied");
      expect(await mainpageUtils.checkInterviewingColumnHeader()).to.equal("Interviewing");
      expect(await mainpageUtils.checkHiredColumnHeader()).to.equal("Hired");
    });

    it('TC 4', async () => { //this test fails because inputs aren't cleared after 'Clear' button is pressed, it's a bug
      await mainpageUtils.filtersClearing();
      expect(await mainpageElements.nameInput.getAttribute('value')).to.equal('');
      expect(await mainpageElements.cityInput.getAttribute('value')).to.equal('');
    });
});