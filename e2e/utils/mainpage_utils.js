const mainpageVariables = require('../variables/mainpage_variables');

const mainpageElements = require('../elements/mainpage_elements');

const mainPageUtils = function() {

    this.getBannerText = async () => {
       return await mainpageElements.appTitle.getText();
    };

    this.setUserName = async (name) => {
        await mainpageElements.nameInput.clear();
        await mainpageElements.nameInput.sendKeys(name);
    };

    this.setCity = async (city) => {
        await mainpageElements.cityInput.clear();
        await mainpageElements.cityInput.sendKeys(city);
    };

    this.checkSubmitBtn = async () => {
        await browser.wait(EC.visibilityOf(mainpageElements.submitBtn), 5000);
        return await mainpageElements.submitBtn.getText();
    };

    this.checkClearBtn = async () => {
        await browser.wait(EC.visibilityOf(mainpageElements.clearBtn), 5000);
        return await mainpageElements.clearBtn.getText();
    };

    this.checkAppliedColumnHeader = async () => {
        await browser.wait(EC.visibilityOf(mainpageElements.appliedColumnHeader), 5000);
        return await mainpageElements.appliedColumnHeader.getText();
    };

    this.checkInterviewingColumnHeader = async () => {
        await browser.wait(EC.visibilityOf(mainpageElements.interviewingColumnHeader), 5000);
        return await mainpageElements.interviewingColumnHeader.getText();
    };

    this.checkHiredColumnHeader = async () => {
        await browser.wait(EC.visibilityOf(mainpageElements.hiredColumnHeader), 5000);
        return await mainpageElements.hiredColumnHeader.getText();
    };

    this.getAppliedColumnLength = async () => {
        await browser.wait(EC.visibilityOf(mainpageElements.appliedColumnHeader), 5000);
        return await mainpageElements.appliedColumnElements.count();
    };

    this.getInterviewingColumnLength = async () => {
        await browser.wait(EC.visibilityOf(mainpageElements.interviewingColumnHeader), 5000);
        return await mainpageElements.interviewingColumnElements.count();
    };

    this.getHiredColumnLength = async () => {
        await browser.wait(EC.visibilityOf(mainpageElements.hiredColumnHeader), 5000);
        return await mainpageElements.hiredColumnElements.count();
    };

    this.filtersClearing = async () => {
        await browser.wait(EC.presenceOf(mainpageElements.filtersForm), 5000);
        await this.setUserName('testName');
        await this.setCity('testCity');
        await mainpageElements.submitBtn.click();
        await mainpageElements.clearBtn.click();
    };
};

module.exports = new mainPageUtils();