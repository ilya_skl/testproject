# Test project. To run it, please follow the instructions:

  - clone this repository
  - ```npm install```
  - run ```./node_modules/protractor/bin/webdriver-manager update```
  - test app should be running on ```localhost:3000```
  - ```npm run test```

## Test cases

**TC 1:** check banner text on the mainpage
**Steps:** open mainpage -> observe the '.App-title' text
**Expected result:** it should be 'OpenOceanStudio: Crew Applications'


**TC 2:** check that submit and clear buttons are present
**Steps:** open mainpage -> observe the filters row
**Expected result:** Submit and Clear button are present with proper buttonText


**TC 3:** check that 3 default columns are present
**Steps:** open mainpage -> observe columns
**Expected result:** Applied, Interviewing and Hired columns are present


**TC 4:** check that Clear button clears Name and City inputs
**Steps:** open mainpage -> enter any values to inputs -> click Submit -> click Clear
**Expected result:** inputs should become cleared

**...and so on.**
