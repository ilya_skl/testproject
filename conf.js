exports.config = {
  allScriptsTimeout: 11000,
  directConnect: true,
  framework: 'mocha',
  capabilities: {
    browserName: 'chrome',
    /*chromeOptions: {
      args: [ "--headless", "--disable-gpu", "--window-size=1920x1080" ]
    },*/
  },
  mochaOpts: {
    reporter: 'spec',
    timeout: 60000
  },
  suites: {
    mainpageTests: ['./e2e/tests/mainpage.spec.js']
  },
  specs: [''],
  baseUrl: 'http://localhost:3000',
  SELENIUM_PROMISE_MANAGER: false,

  onPrepare: () => {
    browser.ignoreSynchronization = true,
    browser.driver.manage().window().maximize(),
    global.EC = protractor.ExpectedConditions,
    browser.manage().timeouts().implicitlyWait(60000);
  }
};
